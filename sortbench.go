package main

import "fmt"
import "time"

type SortAlgo func([]int) []int
type GeneratorAlgo func(int) []int

func removeAlgo(slice []SortAlgo, s int) []SortAlgo {
    return append(slice[:s], slice[s+1:]...)
}

func calcTimeAvg(slice []time.Duration) (seconds float64) {
  cnt:=len(slice)
  var sum float64
  for _,dur := range(slice) {
     sum+=dur.Seconds()
  }
  seconds=sum/float64(cnt)
  return seconds
}

func sortbench(){
  wiederholung:=[]int{1,2,3,4,5,6,7,8,9,10}
  algorithmus:=[]SortAlgo{bubblesort_singlethread,quicksort_singlethread,mergesort_multithread}
  algorithmus_s:=[]string{"bubblesort_singlethread","quicksort_singlethread","mergesort_multithread"}
  verteilung:=[]GeneratorAlgo{genGleichverteilt,genDoubletten,genMonoton,genMonotonReverse}
  verteilung_s:=[]string{"genGleichverteilt","genDoubletten","genMonoton","genMonotonReverse"}

  for vidx,v := range(verteilung) {
    for aidx, a := range(algorithmus) {
      var timeavg float64 //seconds
      for arraylen:=2; timeavg < 60.0; arraylen=arraylen*2 {
        timestat:=make([]time.Duration,0,len(wiederholung))
        for range(wiederholung) {
          unsorted:=v(arraylen)
          start:=time.Now()
          a(unsorted)
          end:=time.Now()
          duration:=end.Sub(start)
          timestat=append(timestat, duration)
        }
        timeavg:=calcTimeAvg(timestat)
        fmt.Println( verteilung_s[vidx], algorithmus_s[aidx], arraylen, timeavg)
      }
    }
  }
}