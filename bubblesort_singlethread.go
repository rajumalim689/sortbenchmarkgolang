//Bubblesort 2 mit vorzeitigem Abbruch bei sortierter Liste

package main

func bubblesort_singlethread(unsorted []int) []int {
  for l:=len(unsorted); l>1; l-- {
    done:=true
    for i:=0; i<l-1; i++ {
      if( unsorted[i] > unsorted[i+1]) {
        done=false
        unsorted[i+1],unsorted[i]=unsorted[i],unsorted[i+1]
      }
    }
    if( done) {
      return unsorted
    }
  }
  return unsorted
}