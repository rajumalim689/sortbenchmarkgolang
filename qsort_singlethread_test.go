package main

import "testing"
import "reflect"
import "fmt"

func cmp(unsort []int, sort []int,t *testing.T ){
  res:=qsort(unsort)
  if ! reflect.DeepEqual(res, sort) {
    errmsg:=fmt.Sprintf("Fail. Wanted %v, got %v\n",unsort, sort)
    t.Fatalf(errmsg)
  }
}

func TestQS1(t *testing.T){
  u:=[]int{3,1,2}
  s:=[]int{1,2,3}
  cmp(u,s,t)
}
func TestQS2(t *testing.T){
  u:=[]int{1,3,1}
  s:=[]int{1,1,1}
  cmp(u,s,t)
}
func TestQS3(t *testing.T){
  u:=[]int{3,1,1}
  s:=[]int{1,1,3}
  cmp(u,s,t)
}
func TestQS4(t *testing.T){
  u:=[]int{1,1,3}
  s:=[]int{1,1,3}
  cmp(u,s,t)
}
