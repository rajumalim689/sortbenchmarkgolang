package main

import "reflect"
import "testing"

func bubbleeq(unsort, sort []int, t *testing.T){
  res:=bubblesort_singlethread(unsort)
  if( !reflect.DeepEqual(res,sort)) {
    t.Fatalf("Fehler, wollte %v habe bekommen %v\n",sort,res)
  }
}

func TestBS1( t *testing.T){
  u:=[]int{3,2,1}
  s:=[]int{1,2,3}
  bubbleeq(u,s,t)
}

func TestBS2( t *testing.T){
  u:=[]int{1,3,1}
  s:=[]int{1,1,3}
  bubbleeq(u,s,t)
}

func TestBS3( t *testing.T){
  u:=[]int{4,1,1,1}
  s:=[]int{1,1,1,4}
  bubbleeq(u,s,t)
}

func TestBS4( t *testing.T){
  u:=[]int{5,1,1,1,1}
  s:=[]int{1,1,1,1,5}
  bubbleeq(u,s,t)
}