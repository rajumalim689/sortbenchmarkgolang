package main

import "math/rand"

func genGleichverteilt(len int64) []int{
  unsorted:=make([]int,len)
  for idx,_:=range(unsorted) {
    unsorted[idx]=rand.Int()
  }
  return unsorted
}

func genDoubletten(len int64) []int{
  unsorted:=make([]int,len)
  for idx,_:=range(unsorted) {
    unsorted[idx]=rand.Int()%4
  }
  return unsorted
}

func genMonoton(arrlen int64) []int{
  if(arrlen <2) {
    arrlen=2
  }
  unsorted:=make([]int,arrlen)
  unsorted[0]=1
  unsorted[1]=-1
  return unsorted
}

func genMonotonReverse(arrlen int64) []int{
  if(arrlen <2) {
    arrlen=2
  }
  unsorted:=make([]int,arrlen)
  unsorted[len(unsorted)-1]=1
  unsorted[len(unsorted)-2]=-1
  return unsorted
}

