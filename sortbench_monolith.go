package main

import "math/rand"
import "fmt"
import "time"
import "bytes"
import "runtime"
import "strconv"
import "sort"
import "math"


func main(){
  sortbench()
}

type SortAlgo func([]int) []int
type GeneratorAlgo func(int64) []int

func removeAlgo(slice []SortAlgo, s int) []SortAlgo {
    return append(slice[:s], slice[s+1:]...)
}

func calcTimeAvg(slice []time.Duration) (seconds float64) {
  cnt:=len(slice)
  var sum float64
  for _,dur := range(slice) {
     sum+=dur.Seconds()
  }
  seconds=sum/float64(cnt)
  return seconds
}
func internal_sort_int_wrapper( unsorted []int) []int {
  sort.Ints(unsorted)
  return unsorted
}

func sortbench(){
  wiederholung:=[]int{1,2,3,4,5,6,7,8,9,10}
  algorithmus:=[]SortAlgo{internal_sort_int_wrapper,bubblesort_singlethread,quicksort_singlethread,mergesort_multithread}
  algorithmus_s:=[]string{"go1.18_stdlib_hybrid_sort","bubblesort_singlethread","quicksort_singlethread","mergesort_multithread"}
  verteilung:=[]GeneratorAlgo{genGleichverteilt,genDoubletten,genMonoton,genMonotonReverse}
  verteilung_s:=[]string{"genGleichverteilt","genDoubletten","genMonoton","genMonotonReverse"}

  for vidx,v := range(verteilung) {
    for aidx, a := range(algorithmus) {
      var timeavg float64 //seconds
      var arraylen int64
      arraylen_label:
      for arraylen=2; timeavg < 60.0; arraylen=arraylen*2 {
        timestat:=make([]time.Duration,0,len(wiederholung))
        for range(wiederholung) {
          unsorted:=v(arraylen)
          start:=time.Now()
          a(unsorted)
          end:=time.Now()
          duration:=end.Sub(start)
          timestat=append(timestat, duration)
        }
        timeavg:=calcTimeAvg(timestat)
        logarraylen:=int32( math.Round(math.Log2(float64(arraylen))))
        fmt.Println( verteilung_s[vidx], algorithmus_s[aidx], "2^",logarraylen, "=", arraylen, "time=",timeavg,"s")
        if(timeavg >3.0) {
          break arraylen_label
        }
      }
    }
  }
}



func getGID() uint64 {
    b := make([]byte, 64)
    b = b[:runtime.Stack(b, false)]
    b = bytes.TrimPrefix(b, []byte("goroutine "))
    b = b[:bytes.IndexByte(b, ' ')]
    n, _ := strconv.ParseUint(string(b), 10, 64)
    return n
}




func quicksort_singlethread(a []int) []int {
  if len(a) < 2 { return a }

  left, right := 0, len(a) - 1

  // Pick a pivot
  pivotIndex := rand.Int() % len(a)

  // Move the pivot to the right
  a[pivotIndex], a[right] = a[right], a[pivotIndex]

  // Pile elements smaller than the pivot on the left
  for i := range a {
    if a[i] < a[right] {
      a[i], a[left] = a[left], a[i]
      left++
    }
  }

  // Place the pivot after the last smaller element
  a[left], a[right] = a[right], a[left]

  // Go down the rabbit hole
  quicksort_singlethread(a[:left])
  quicksort_singlethread(a[left + 1:])


  return a
}

