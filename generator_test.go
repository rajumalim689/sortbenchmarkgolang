package main

import "testing"
import "fmt"
import "time"

func Test1(t *testing.T){
  u:=genGleichverteilt(10)
  fmt.Println(u)
}
func Test2(t *testing.T){
  u:=genGleichverteilt(2)
  fmt.Println(u)
}
func Test3(t *testing.T){
  u:=genDoubletten(10)
  fmt.Println(u)
}
func Test4(t *testing.T){
  u:=genDoubletten(2)
  fmt.Println(u)
}

func Test5(t *testing.T){
  u:=genMonoton(10)
  fmt.Println(u)
}

func Test6(t *testing.T){
  u:=genMonoton(2)
  fmt.Println(u)
}

func Test7(t *testing.T){
  start := time.Now()
  q:=make(chan int,1)
  q<-1
  <-q
  elapsed := time.Since(start)
//  format:="2006-01-02T15:04:05.999999-07:00"
//  ti,_:=time.Parse(format,elapsed)
  time.Sleep(751 * time.Millisecond)
  fmt.Printf("Zeit vergangen %s",elapsed)
}