package main

import "testing"
import "time"

func TestTimeAvg(t *testing.T){
  duration_list:=[]time.Duration{1,2,3}
  expect:=2.0
  result:=calcTimeAvg(duration_list)
  if( expect != result){
    t.Fatalf("Zeitdurchschnitt nicht wie erwatet")
  }
}